module.exports = {
  apps: [{
    name: 'protopia_bot_new',
    script: 'npm',
    args: 'run prod',
    watch: true,
    ignore_watch: ['*/**/data/*.*'],
  }],
};
