import {
  ApolloClient, HttpLink, ApolloLink,
  InMemoryCache, gql,
} from '@apollo/client/core';
import { onError } from '@apollo/client/link/error';
import fetch from 'cross-fetch';
import { setContext } from 'apollo-link-context';

import config from '../config/config';

const jose = require('node-jose');

let clientToken;

const errorLink = onError(({
  graphQLErrors, networkError,
}) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message, extensions }) => {
      if (extensions.code !== 'FORBIDDEN') {
        console.error(message);
      }
    });
    console.error(JSON.stringify(graphQLErrors, null, 2));
  }
  if (networkError && networkError.statusCode !== 400) {
    console.error(`[Network error]: ${networkError}`);
  }
  return null;
});

const link = new HttpLink({
  uri: config.server_url, fetch,
});

const authLink = setContext((_, prevContext) => {
  const token = prevContext.botContext && prevContext.botContext.userToken && !prevContext.asClient ? prevContext.botContext.userToken : clientToken;
  return {
    headers: {
      ...prevContext.headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  };
});

const client = new ApolloClient({
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    mutate: {
      errorPolicy: 'all',
    },
  },
  cache: new InMemoryCache(),
  link: ApolloLink.from([errorLink, authLink, link]),
});

const getUser = async (ctx) => {
  if (!ctx.userToken) {
    return null;
  }
  const user = await client.query({
    context: { botContext: ctx },
    query: gql`
            query {
                userInfo {
                    _id name family_name email roles
                }
            }
        `,
  });

  return user.data.userInfo;
};

const clientAuth = async () => {
  const clientTokenResult = await client.mutate({
    mutation: gql`
            mutation token($input: TokenInput) {
                token(input: $input) {
                    access_token
                    token_type
                    expires_in
                    refresh_token
                    __typename
            }
        }
    `,
    variables: {
      input: {
        assertion: config.assertion_token,
        grant_type: 'jwt-bearer',
      },
    },
  });
  clientToken = clientTokenResult.data.token.access_token;
};

const userAuth = async (ctx, next) => {
  const telegramUserId = ctx.update.message ? ctx.update.message.from.id : ctx.update.callback_query.from.id;
  if (config.dev_user_id && config.dev_user_id !== telegramUserId) {
    throw new Error('User has not access to bot');
  }

  try {
    const lifeTime = 60 * 60;
    const issued_at = Date.now();
    const expires_at = issued_at + lifeTime;
    const idTokenClaims = {
      sub: telegramUserId,
      aud: [config.client_url, config.client_url],
      iss: config.client_url,
      acr: 'telegram',
      iat: issued_at,
      eat: expires_at,
    };
    const idTokenPayload = Buffer.from(JSON.stringify(idTokenClaims));
    const idTokenKey = await jose.JWK.asKey({
      kid: config.client_id,
      kty: 'oct',
      k: config.client_secret,
    });
    const idToken = await jose.JWS.createSign({ format: 'compact' }, idTokenKey)
      .update(idTokenPayload)
      .final();
    const userAuthResult = await client.mutate({
      mutation: gql`
              mutation authorize($input: AuthorizeInput!) {
                  authorize(input: $input) {
                      auth_req_id
              }
          }
      `,
      variables: {
        input: {
          scope: ['user'],
          assertion: config.assertion_token,
          id_token_hint: idToken,
        },
      },
    });
    const userTokenResult = await client.mutate({
      mutation: gql`
              mutation token($input: TokenInput) {
                  token(input: $input) {
                      access_token
                      token_type
                      expires_in
                      refresh_token
                      __typename
              }
          }
      `,
      variables: {
        input: {
          assertion: config.assertion_token,
          grant_type: 'ciba',
          auth_req_id: userAuthResult.data.authorize.auth_req_id,
        },
      },
    });
    ctx.userToken = userTokenResult.data.token.access_token;
  } catch (e) {
    //
  }

  ctx.graphql = client;
  ctx.getUser = () => getUser(ctx);
  await next();
};

export { clientAuth };
export default userAuth;
