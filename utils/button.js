const { Markup } = require('telegraf');

const getPageButtons = (page, pages) => {
  let pageNumbers = [];
  if (page === 0) {
    pageNumbers = [0, page + 1, page + 2, page + 3, pages - 1];
  } else if (page === 1) {
    pageNumbers = [0, page, page + 1, page + 2, pages - 1];
  } else if (page === pages - 2) {
    pageNumbers = [0, page - 2, page - 1, page, pages - 1];
  } else if (page === pages - 1) {
    pageNumbers = [0, page - 3, page - 2, page - 1, pages - 1];
  } else {
    pageNumbers = [0, page - 1, page, page + 1, pages - 1];
  }
  pageNumbers = pageNumbers.filter((value, index) => pageNumbers.indexOf(value) === index);
  const pageButtons = [];
  pageNumbers.forEach((number, key) => {
    if (number < 0 || number >= pages) {
      return;
    }
    let title;
    if (number === page) {
      title = `-${number + 1}-`;
    } else if (key === 0) {
      title = `<<${number + 1}`;
    } else if (number === pages - 1) {
      title = `${number + 1}>>`;
    } else if (number < page) {
      title = `<${number + 1}`;
    } else if (number > page) {
      title = `${number + 1}>`;
    }

    pageButtons.push({ number, title });
  });

  return pageButtons;
};

const buttonsWithPaging = async (options) => {
  const page = parseInt(options.page) || 0;
  const perPage = 3;
  const pages = Math.ceil(options.buttons.length / perPage);
  let buttons = options.buttons;
  buttons = buttons.slice(perPage * page, perPage * (page + 1));
  const buttonsMarkup = buttons.map((button) => [Markup.button.callback(button.title, `${options.prefix}|${button.id}`)]);

  const pageButtons = getPageButtons(page, pages).map(
    (pageButton) => Markup.button.callback(pageButton.title, `${options.pagePrefix}|${pageButton.number}`),
  );

  buttonsMarkup.push(pageButtons);
  options.ctx[options.edit ? 'editMessageText' : 'replyWithHTML'](
    options.message,
    { parse_mode: 'html', ...Markup.inlineKeyboard(buttonsMarkup) },
  );
};

const buttons = async (options) => {
  let buttons = options.buttons;
  const buttonsMarkup = buttons.map((buttonsLine) => (
    buttonsLine.map((button) => (
      Markup.button.callback(button.title, `${options.prefix}|${button.id}`)
    ))));
  options.ctx[options.edit ? 'editMessageText' : 'replyWithHTML'](
    options.message,
    { parse_mode: 'html', ...Markup.inlineKeyboard(buttonsMarkup) },
  );
};

const listWithPaging = async (options) => {
  const page = parseInt(options.page) || 0;
  const perPage = options.perPage || 3;
  const pages = Math.ceil(options.list.length / perPage);
  let list = options.list;
  list = list.slice(perPage * page, perPage * (page + 1));

  const pageButtons = getPageButtons(page, pages).map(
    (pageButton) => Markup.button.callback(pageButton.title, `${options.pagePrefix}|${pageButton.number}`),
  );

  const buttonsMarkup = [pageButtons];
  options.ctx[options.edit ? 'editMessageText' : 'replyWithHTML'](
    `${options.message ? `${options.message}\n\n` : ''
    }${list.join('\n')}`,
    { parse_mode: 'html', ...Markup.inlineKeyboard(buttonsMarkup) },
  );
};

export { buttonsWithPaging, buttons, listWithPaging };
