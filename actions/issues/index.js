import fetch from 'cross-fetch';
import { listWithPaging } from '../../utils/button';
import { compareStrings } from '../../utils/nlp';
import configIssues from './config_issues';

const showIssues = async (ctx, edit, page) => {
  const issues = await (await fetch(`https://gitlab.com/api/v4/projects/${configIssues.gitlab_project_id}/issues?state=opened&per_page=100`)).json();

  listWithPaging({
    ctx,
    edit,
    page,
    perPage: 20,
    pagePrefix: 'issue-page',
    list: issues.map((issue) => `* <a href="${issue.web_url}">${issue.title}</a>`),
  });
};

const issuesActions = (bot) => {
  bot.command('issues', async (ctx) => {
    showIssues(ctx);
  });

  bot.action(/^issue-page\|(.*)$/, async (ctx) => {
    await ctx.answerCbQuery();
    showIssues(ctx, true, ctx.match[1]);
  });

  bot.on('text', async (ctx, next) => {
    if (compareStrings(ctx.message.text, 'бот задачи')) {
      showIssues(ctx);
    } else {
      await next();
    }
  });
};

export default issuesActions;
