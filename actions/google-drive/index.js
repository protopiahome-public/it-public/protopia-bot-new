import { google } from 'googleapis';
import fs from 'fs';
import moment from 'moment';
import { listWithPaging } from '../../utils/button';
import gdriveConfig from './config';

let CronJob = require('cron').CronJob;

const auth = new google.auth.GoogleAuth({
  keyFile: `${__dirname}/../../config/google-service-token.json`,
  scopes: ['https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/drive.metadata.readonly',
    'https://www.googleapis.com/auth/drive.appdata',
    'https://www.googleapis.com/auth/drive.metadata',
    'https://www.googleapis.com/auth/drive.photos.readonly'],
});
const drive = google.drive({ version: 'v3', auth });

const search = async (ctx, query, edit, page) => {
  const user = await ctx.getUser();
  if (!user || user.roles.includes('guest')) {
    return false;
  }

  drive.files.list({
    q: `fullText contains '${query}'`,
    fields: 'nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)',
    spaces: 'drive',
    pageSize: 1000,
  }, (err, res) => {
    if (err) {
      console.error(err);
    } else {
      listWithPaging({
        ctx,
        edit,
        page,
        message: `Запрос: ${query}`,
        perPage: 20,
        pagePrefix: 'gd-page',
        list: res.data.files.map((file) => `* <a href="https://drive.google.com/open?id=${file.id}">${file.name}</a>`),
      });
    }
  });
};

const changes = async (bot) => {
  const lastPath = `${__dirname}/data/last.json`;
  if (!fs.existsSync(lastPath)) {
    fs.writeFileSync(lastPath, JSON.stringify({
      time: (new Date()).toISOString(),
    }));
  }
  const last = JSON.parse(fs.readFileSync(lastPath, 'utf-8'));
  if (!last.time) {
    fs.writeFileSync(lastPath, JSON.stringify({
      time: (new Date()).toISOString(),
    }));
    return;
  }

  const fields = 'changes(file(id,name,parents,modifiedTime,lastModifyingUser(displayName)))';
  const pageToken = await drive.changes.getStartPageToken({
    spaces: 'drive',
    includeTeamDriveItems: true,
    supportsTeamDrives: true,
    includeRemoved: true,
    // fields,
  });
  const changes = await drive.changes.list({
    pageToken: pageToken.data.startPageToken - 50,
    pageSize: 50,
    spaces: 'drive',
    includeTeamDriveItems: true,
    supportsTeamDrives: true,
    includeRemoved: true,
    fields,
  });
  let newTime;
  changes.data.changes.sort((change1, change2) => (new Date(change1.file.modifiedTime) - new Date(change2.file.modifiedTime)));
  for (const k in changes.data.changes) {
    const change = changes.data.changes[k];
    if (new Date(change.file.modifiedTime) <= new Date(last.time)) {
      continue;
    }
    newTime = new Date(change.file.modifiedTime);
    const parent = change.file.parents ? await drive.files.get({ fileId: change.file.parents[0] }) : null;
    bot.telegram.sendMessage(gdriveConfig.notifyChannel, `
${change.file.name}
${(change.file.lastModifyingUser && change.file.lastModifyingUser.displayName) || ''}
В папке: ${parent ? parent.data.name : ''}
https://drive.google.com/open?id=${change.file.id}
${moment(change.file.modifiedTime).format('DD.MM.YYYY HH:mm:ss')}
`);
  }

  if (newTime) {
    last.time = newTime.toISOString();

    fs.writeFileSync(lastPath, JSON.stringify(last));
  }
};

const driveActions = (bot) => {
  bot.hears(/^\/search( ((.|\n)+))?$/m, async (ctx) => {
    if (!ctx.match[2]) {
      ctx.reply('Поиск вызывается командой\n/search Текст запроса');
    } else {
      search(ctx, ctx.match[2]);
    }
  });

  bot.action(/^gd-page\|(.*)$/, async (ctx) => {
    await ctx.answerCbQuery();
    search(ctx, ctx.update.callback_query.message.text.match(/^Запрос: ((.|\n)+)\n\n/m)[1], true, ctx.match[1]);
  });

  bot.command('gdchanges', async (ctx) => {
    changes(ctx);
  });

  if (gdriveConfig.notifyEnabled) {
    let changesJob = new CronJob('0 * * * * *', () => changes(bot));
    changesJob.start();
  }
};

export default driveActions;
