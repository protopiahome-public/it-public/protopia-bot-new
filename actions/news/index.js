import {
  gql,
} from '@apollo/client/core';
import { stringToWords, stringToParts } from '../../utils/nlp';
import config from '../../config/config';
import newsConfig from './news_config';
import { getCircle } from '../../utils/s3';

const addNews = async (ctx, text) => {
  const circle = await getCircle(ctx);
  const news = await ctx.graphql.mutate({
    context: { botContext: ctx },
    mutation: gql`
        mutation changeNewsItem($input:NewsItemInput){
          changeNewsItem(input: $input) {
              _id
            }
        }
    `,
    variables: {
      input: {
        title: text,
        date: new Date().toISOString(),
        circle_id: circle ? circle._id : null,
      },
    },
  });
  ctx.reply(`Новость добавлена. Список новостей: ${config.web_panel_url}/calendar/domain/newslist\n\n`
  + `Отредактировать новость: ${config.web_panel_url}/calendar/domain/newsedit/${news.data.changeNewsItem._id}`);
  const user = await ctx.getUser();
  const circleText = circle._id ? ` (круг ${circle.title})` : '';
  ctx.tg.sendMessage(newsConfig.news_channel_id, `Новость от ${user.name} ${user.family_name}${circleText}:\n${text}`);
};

const newsAction = (bot) => {
  bot.on('text', async (ctx, next) => {
    const words = stringToWords(ctx.message.text);
    if (words[0] === 'бот' && words[1] === 'новость') {
      const text = stringToParts(ctx.message.text).slice(4).join('').trim();
      if (!text) {
        ctx.reply('Напишите текст новости.');
      } else {
        addNews(ctx, text);
      }
    } else {
      await next();
    }
  });
};

export default newsAction;
