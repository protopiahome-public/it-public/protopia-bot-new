import {
  gql,
} from '@apollo/client/core';
import { stringToWords, stringToParts, compareStrings } from '../../utils/nlp';

const getChat = async (ctx) => {
  const query = async () => {
    const result = await ctx.graphql.query({
      context: { botContext: ctx, asClient: true },
      query: gql`
                  query getTgChatByTelegramId($telegram_id: String!) {
                      getTgChatByTelegramId(telegram_id: $telegram_id) {
                          _id title welcome_message
                      }
                  }
              `,
      variables: {
        telegram_id: ctx.message.chat.id.toString(),
      },
    });
    return result.data.getTgChatByTelegramId ? result.data.getTgChatByTelegramId : null;
  };

  let tgChat = await query();

  if (!tgChat) {
    await ctx.graphql.mutate({
      context: { botContext: ctx, asClient: true },
      mutation: gql`
          mutation changeTgChat($input:TgChatInput){
            changeTgChat(input: $input) {
              _id
            }
          }
      `,
      variables: {
        input: {
          title: ctx.message.chat.title,
          chat_id: ctx.message.chat.id,
        },
      },
    });
    tgChat = await query();
  }

  return tgChat;
};

const changeWelcomeMessage = async (ctx, welcomeMessage) => {
  const tgChat = await getChat(ctx);
  await ctx.graphql.mutate({
    context: { botContext: ctx },
    mutation: gql`
        mutation changeTgChat($_id: ID, $input:TgChatInput){
          changeTgChat(_id: $_id, input: $input) {
            _id
          }
        }
    `,
    variables: {
      _id: tgChat._id,
      input: {
        welcome_message: welcomeMessage,
      },
    },
  });
};

const moderationActions = (bot) => {
  bot.on('new_chat_members', async (ctx) => {
    const welcome_message = (await getChat(ctx)).welcome_message;
    if (welcome_message) {
      ctx.message.new_chat_members.forEach((member) => ctx.reply(
        welcome_message.replace(
          '$user', `${member.first_name || ''} ${member.second_name || ''}`,
        ),
      ));
    }
  });

  bot.on('text', async (ctx, next) => {
    if (compareStrings(ctx.message.text, 'бот удалить приветствие')) {
      await changeWelcomeMessage(ctx, '');
      ctx.reply('Приветствие удалено.');
    } else {
      await next();
    }
  });

  bot.on('text', async (ctx, next) => {
    const words = stringToWords(ctx.message.text);
    if (words[0] === 'бот' && words[1] === 'приветствие') {
      const text = stringToParts(ctx.message.text).slice(4).join('').trim();
      if (!text) {
        ctx.reply('Напишите текст приветствия.');
      } else {
        await changeWelcomeMessage(ctx, text);
        ctx.reply('Текст приветствия добавлен');
      }
    } else {
      await next();
    }
  });

  bot.command('donate', async (ctx) => {
    ctx.reply(`
Цифровая Протопия будет рада вашей помощи в разработке открытого ПО:

Банковская карта (ВТБ):
2200 2407 5706 1466
    `);
  });
};

export default moderationActions;
