import { google } from 'googleapis';
import { compareStrings } from '../../utils/nlp';
import configMoney from './config_money';

const auth = new google.auth.GoogleAuth({
  keyFile: `${__dirname}/../../config/google-service-token.json`,
  scopes: ['https://www.googleapis.com/auth/spreadsheets.readonly'],
});

const showMoney = async (ctx) => {
  const user = await ctx.getUser();
  if (!user || user.roles.includes('guest')) {
    return false;
  }

  const sheets = google.sheets({ version: 'v4', auth });
  sheets.spreadsheets.values.get({
    spreadsheetId: configMoney.spreadsheetId,
    range: configMoney.range,
  }, (err, res) => {
    if (err) return console.log(`The API returned an error: ${err}`);
    const rows = res.data.values;
    if (rows.length) {
      ctx.reply(`${rows[0][0]}: ${rows[1][0]}\n${rows[0][1]}: ${rows[1][1]}`);
    } else {
      console.log('No data found.');
    }
  });
};

const moneyActions = (bot) => {
  bot.command('money', async (ctx) => {
    showMoney(ctx);
  });

  bot.on('text', async (ctx, next) => {
    if (compareStrings(ctx.message.text, 'бот деньги')) {
      showMoney(ctx);
    } else {
      await next();
    }
  });
};

export default moneyActions;
