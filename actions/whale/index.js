import mysql from 'mysql';
import whaleConfig from './whale_config.json';
import { compareStrings } from '../../utils/nlp';

let connection;
if (whaleConfig.mysql_host) {
  connection = mysql.createConnection({
    host: whaleConfig.mysql_host,
    user: whaleConfig.mysql_user,
    password: whaleConfig.mysql_password,
    database: whaleConfig.mysql_db,
  });
  connection.connect();
}

const whale = async (ctx) => {
  connection.query('SELECT *, CONCAT(\'https://vk.com/wall-74217691_\', vk_id) as url FROM whale', (error, posts, fields) => {
    if (error) throw error;
    posts.forEach((post) => {
      post.json = JSON.parse(post.json);
      if (post.json) {
        if (post.json.attachments) {
          post.json.attachments.some((attachment) => {
            if (attachment.type === 'photo') {
              const biggerImageType = ['src_xxxbig', 'src_xxbig', 'src_xbig', 'src_big'].find((imageType) => !!attachment.photo[imageType]);
              if (biggerImageType) {
                post.text += `\n\n${attachment.photo[biggerImageType]}`;
                return true;
              }
            }
            return false;
          });
        }
      }
    });
    const post = posts[Math.floor(Math.random() * posts.length)];
    post.text = post.text.replaceAll('<br>', '\n');
    post.text += `\n\n${post.url}`;
    ctx.reply(post.text);
  });
};

const whaleActions = (bot) => {
  bot.command('whale', (ctx) => whale(ctx));

  bot.on('text', async (ctx, next) => {
    if (compareStrings(ctx.message.text, 'бот покажи чудо')) {
      whale(ctx);
    } else {
      await next();
    }
  });

  bot.on('sticker', async (ctx, next) => {
    if (ctx.message.sticker.set_name === 'ProtopiaBotManage' && ctx.message.sticker.file_unique_id === 'AgADGgAD7PbvEQ') {
      whale(ctx);
    } else {
      await next();
    }
  });
};

export default whaleActions;
